# titactoePython

A titactoe minigame in which you can play against an IA based on minimax alpha-beta pruning algorithm. The AI will be represented by "the machine" (player 1) and you will embody the human (player -1). The player who starts is chosen randomly, the game ends either when someone managed to complete a row/column/diagonal or when the board is full.


## Usage
You can start it using python3 in shell or using some specific IDE (Thonny for example)
```sh 
titactoePython$ python3 tictactoe.py
```


## Authors and acknowledgment
### Authors
- Nassim Guinet


## Project status
There is no plan for future update on this project. We can see improvement in the visuals by replacing 1/-1 with X's and O's